import { html, LitElement } from 'lit-element';
import style from './sellfone-main-footer-styles.js';

class SellfoneMainFooter extends LitElement {
  static get properties() {
    return {
      iconsArray:{
        type:Array,
        attribute:"icons-array"
      },
      linksArray:{
        type:Array,
        attribute:"links-array"
      },
      aboutUsLinks:{
        type:Array,
        attribute:"about-us-links"
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.iconsArray=[
      "http://pngimg.com/uploads/paypal/paypal_PNG22.png",
      "https://stickeroid.com/uploads/pic/full-pngimg/thumb/stickeroid_5bf57e6a76020.png",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/MasterCard_Logo.svg/1280px-MasterCard_Logo.svg.png",
      "https://about.americanexpress.com/sites/americanexpress.newshq.businesswire.com/files/logo/image/American-Express-Logotype-Stacked.png",
      "http://www.freelogovectors.net/wp-content/uploads/2019/02/Mercadopago-logo.png",
      "https://www.londonkapital.com/img/bank-transfer.png"
    ]
    this.linksArray=[
      {title:"Iphone"},
      {title:"phone 2"},
      {title:"Samsung"},
      {title:"Huawei"},
      {title:"phone"},
      {title:"phone"}
    ];
    this.aboutUsLinks=[
      {title:"Acerca de nosotros"},
      {title:"Preguntas frecuentes"},
      {title:"Sellfone es confiable?"},
      {title:"Blog"},
      {title:"Como Vender?"},
      {title:"Compo comprar?"}
    ]
  }

  render() {
    return html`
        <div class="footerComponent">
            <div class="paymentWaysContainer">
                ${this.iconsArray.map(image=>{
                  return html`<img src="${image}" class="iconImage" alt="image">`
                })}
            </div>

              <div class="linksContainer">
                <div class="aboutUsContainer">
                <p class="subTitle">Sellfone</p>
                  <div class="linkContainer">
                    ${this.aboutUsLinks.map(link=>html`<a class="link" href="#">${link.title}</a>`)}
                   </div>
                </div>

                <div class="mailContainer">
                 <div class="newsLetterContainer">
                    <label class="emailLabel" for="email">Suscribete y recibe las mejores ofertas y noticias!</label>
                    <input class="newsletter">
                    <button class="newsletterButton">Enviar</button>
                 </div>
                </div>

            </div>


        </div>
      `;
    }
}

window.customElements.define("sellfone-main-footer", SellfoneMainFooter);
