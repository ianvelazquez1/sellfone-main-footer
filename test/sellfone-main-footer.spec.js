/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-main-footer.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-main-footer></sellfone-main-footer>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
